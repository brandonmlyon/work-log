# Boards

### [DE Kanban](https://gitlab.com/groups/gitlab-com/-/boards/2415063) || [DE Sprint](https://gitlab.com/groups/gitlab-com/-/boards/2415116)

[Sprint New](https://gitlab.com/groups/gitlab-com/-/boards/1761580?scope=all&utf8=%E2%9C%93&label_name[]=mktg-inbound&not[label_name][]=mktg-status%3A%3Atriage&not[label_name][]=mktg-status%3A%3Ablocked) || [Sprint Classic](https://gitlab.com/groups/gitlab-com/-/boards/1483370?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-website&not[label_name][]=mktg-status%3A%3Atriage&label_name[]=mktg-inbound&assignee_username=brandon_lyon) || [Inbound leaders](https://gitlab.com/groups/gitlab-com/-/boards/2371969) || [DS sprint](https://gitlab.com/groups/gitlab-com/-/boards/2248456?label_name[]=mktg-website%3A%3Adesign-system&label_name[]=mktg-inbound&label_name[]=mktg-website)

# Sticky tasks

[Metrics](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/287) || [Localization](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/271) || [Slippers](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/218) || [CMS](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/192) || [Commit template](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/275) || [Cookiebot](https://gitlab.com/groups/gitlab-com/-/epics/681) || [Marketo](https://gitlab.com/groups/gitlab-com/-/epics/699) || [www DRI](https://gitlab.com/gitlab-com/Product/-/issues/1869)

# Quick links

[Example epic](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/192) || [Example issue](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/546) || [Page update](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-update-webpage-brief) || [New page](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-new-website-brief) || [Other](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-website-other) || [Bug report](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=-website-bug-report)

# Daily template

```
# DAY 2021-00-00

##### Meetings

* List.

##### Work

* Things I planned to do today.
* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
* Check your slack threads, DMs, browser history, and gitlab activity.
```

# [Previous month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2021/02.md) || [Next month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2021/04.md)

<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->

# WED 2021-03-31

##### Work

* [Feedback re job family headcount request MRs](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11170).
* [Metrics OKR status update](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/308#note_541441518).
* [Hotjar UX pages](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1196).
* [New pricing page test request](https://gitlab.com/gitlab-com/marketing/inbound-marketing/marketing-website/-/issues/108).
* Review MRs.
* Documentation.
* Slack re staging environment.

# TUE 2021-03-30

##### Meetings

* Weekly 1:1.
* Sync re pricing test.

##### Work

* Assist JG with javascript bug.
* [New AB pricing test request from SS](https://gitlab.com/gitlab-com/marketing/inbound-marketing/marketing-website/-/issues/102).
* [Heatmaps](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1196).
* Slack re Vue + Slippers + Middleman + Vue SSR.
* Documentation case.
* [Feedback re job family headcount request MRs](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11170).
* Review and assist with MRs.
* Slack with MP re OKRs.
* Slack re [Cloudflare](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8016).

# MON 2021-03-29

##### Meetings

* OKR feature kickoff.
* Sync with Ash.
* Sprint planning.
* Sync with CW.

##### Work

* Documentation case.
* Sprint planning prep.
* 1:1 prep.
* [Bruce](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1185).
* Slack re SSE used in handbook.
* Monitor AB test results.
* [Commit](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1118).

# FRI 2021-03-26

##### Work

* [Feedback re job family headcount request MRs](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11170).
* [Marketo landing page agency task](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1067).
* [Historical AB test research](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1189).
* Review MRs.

# THU 2021-03-25

##### Meetings

* IM recap.
* Sprint release.
* Sprint retro.

##### Work

* Pricing AB test.
* Troubleshoot pipeline bugs.
* Help HS with MRs.
* [Metrics OKR](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1192).
* Refer writer to open positions slack with DM and BM.

# WED 2021-03-24

##### Meetings

* Smartling sync.

##### Work

* [Pricing AB test](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1053).
* [Feature flag procurement updated subscription](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/13#note_537020764).
* Help TN with handbook MR.
* [Fix the Contribute page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78142).
* [Fix button styles on the marketing handbook page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78145).
* Advise LD re Marketo forms.
* [Collaborate on image size definitions](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1173).
* [Review blog template mega MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/77693).
* Review MRs.
* [Promoted OKR issue to epic & iterated refinement](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/308).

# TUE 2021-03-23

##### Meetings

* UI to code working group.
* Weekly 1:1.

##### Work

* [OKR updates](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/514).
* [Blog MR review](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/77693).
* [Bug sleuthing re post attribution](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1185).
* Slack re idea to prepent OKR epics with 🟥 🟨 🟩
* Slack re button color followup from UI to code group meeting.
* [MR to fix duplicate release section on homepage](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78049).
* Review other MRs.
* [Review navigation prototype](https://www.figma.com/file/NNDMereDKvqvtpOzqjy0X4/Navigation?node-id=174%3A0).

# MON 2021-03-22

##### Meetings

* Smartling engineering sync.
* Sync with LB.

##### Work

* Slack facilitate fix for homepage release section.
* Slack re DataStudio for blog.
* Slack re Commit blocks, timeline, and logos/speakers.
* Weekly planning.
* [GDN connection](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1033).
* Review MRs.

# PTO 2021-03-19

* Family & Friends day

# THU 2021-03-18

##### Meetings

* Localization sync.
* Marketing weekly.
* DM skip level 1:1.
* Commit sync.

##### Work

* Review MRs.
* [AB test 202103a](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1053).
* [Localization DE](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1164).
* [Localization GDN](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1033#note_532795130).
* [Contribute bugfix](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/77767).

# WED 2021-03-17

##### Meetings

* Coffee chat with GM.
* Opengraph sync with Brand.

##### Work

* Tracked down analytics for feature flag discrepancy.
* Slack re increase in feature flag MAU allotment.
* [Started working on pricing test 202103a](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1053).
* Slack re UX of future pricing tests.
* Slack re mobile UX.
* Slack re private MR fork.
* Slack re pricing copy.
* Review MRs.
* Bug fix for experiment framework viewport height.
* Multiple status updates to stakeholders for localization GDN.
* Prepare for skip-level.

# TUE 2021-03-16

##### Meetings

* Weekly 1:1.

##### Work

* Slack from CW in `#website` re monorepo performance.
* [Product feedback](https://gitlab.com/gitlab-org/gitlab/-/issues/229731#note_530707118).
* Review MRs.
* File issues for this sprint.
* Catch up from PTO.
* Refine issue boards.

# MON 2021-03-15

##### Meetings

* Sprint planning.
* Sync with LB and MP re contractors.
* Cofee chat with LB.

##### Work

* Prep sprint plan.
* Prep 1:1 agenda.
* Catch up from PTO.
* Review MRs.
* [LaunchDarkly procurement](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/13).
* [RSS feeds](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/4287#note_529790029).
* [Marketo LP imagery](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/3839#note_529788366).
* Filed issues I thought of while on PTO such as [Swiftype analytics](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1151).
* [Review events template progress](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/275).
* [AR for DE engineering Marketo access](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1144).
* [Outlined more issues for Benchmarking Exercise](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/514).
* [Status update for Benchmarking Exercise](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/514).

# RTO 2021-03-12

* Spring RTO

# RTO 2021-03-11

* Spring RTO

# RTO 2021-03-10

* Spring RTO

# RTO 2021-03-09

* Spring RTO

# RTO 2021-03-08

* Spring RTO

# FRI 2021-03-05

##### Meetings

* Marketo DE engineering workshop.

##### Work

* Workshop prep.
* Edit workshop recording.
* [Child topics migration](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/75748).
* Fix canonical bugs in child topics migration.
* Review MRs.
* Slack re localization.

# THU 2021-03-04

##### Meetings

* Localization sync.
* Marketing weekly.
* LB sync.

##### Work

* Prep for workshop.
* [Homepage promo](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1109).
* [Localization](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1029).
* [Repo DRI](https://gitlab.com/groups/gitlab-com/-/epics/1370#note_522488766).
* [Localization lang attributes](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1108).
* Slack re build tokens.
* Assist with MRs.
* Slack re Netlify.

# WED 2021-03-03

##### Work

* [Review Slippers MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/75286#note_521557945).
* [Marketo bugfix](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1062).
* Review Partners MR.
* [Make localization resource section easier to edit](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1028).
* Assist with git workshop.
* Slack re motion in DS.
* Review team recordings.

# TUE 2021-03-02

##### Meetings

* Marketing annual awards & FY 22 plan.
* Weekly 1:1.
* Testing strategy sync with SM.

##### Work

* Review Typeform MR.
* Topic child migration.
* Review MRs.
* Slack re DS headline links.
* Slack re expense reports.
* Slack re slippers node troubleshooting.
* Slack re navigation UX.

# MON 2021-03-01

##### Meetings

* Sprint planning.

##### Work

* Prep for sprint planning.
* Prep for 1:1.
* Review MRs.
* [Review blog post template plans](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/996).
* [Prep the Typeform issue](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1039).
* [Refine the Commit issue](https://www.google.com/url?q=https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/275).
* Update work log for past week.
* [Refine metrics OKR epic](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/514).

# DAY 2021-00-00

# [Previous month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2021/02.md) || [Next month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/04.md)

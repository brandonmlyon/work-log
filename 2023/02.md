# Weekly Template

```
# WEEK ENDING THURSDAY 2023-01-00

##### Overtime

* Note if there has been any overtime

##### PTO

* Note if the week is less than 5 days

##### Meetings

* Number of hours impacted by meetings

##### Work

* Things I planned to do this week.
* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
* Check your...
    * Slack threads
    * DMs
    * Mentions
    * Email
    * Google Docs activity
    * Figma activity
    * Browser history
    * Calendar
    * Issue boards
    * Commit history
    * Code reviews
```

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2023/01.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2023/03.md)

# Work Log

<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->

# WEEK ENDING THURSDAY 2023-02-23

##### PTO

* 8hr

##### Meetings

* 2hr

##### Work

* SS PoC

# WEEK ENDING THURSDAY 2023-02-16

##### Overtime

* 5hr

##### Meetings

* 2hr

##### Work

* Ahrefs audit
* Install Vercel Audiences (analytics)
* Docs site assistance
* Pricing page discussions
* Updated default open graph images
* Analytics dashboards for solutions, enterprise, and blog posts
* Figma design debt
* Update nav menu
* Bug with card list gap spacing

# WEEK ENDING THURSDAY 2023-02-09

##### Overtime

* 3hr

##### Meetings

* 3hr

##### Work

* Homepage updates
* Testimonial component
* Add rich text support to icon card lists
* Stream workers page
* Training others

# WEEK ENDING THURSDAY 2023-02-02

##### Meetings

* 4hr

##### Work

* Solutions pages
* Training others
* Template for gated content
* Content library card images
* Mobile bugfix for demo page

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2023/01.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2023/03.md)

# Weekly Template

```
<!----------------------------->
# WEEK ENDING FRIDAY 2023-01-00

##### Overtime

* Note if there has been any overtime

##### PTO

* Note if the week is less than 5 days

##### Meetings

* Number of hours impacted by meetings

##### Work

* Things I planned to do this week.
* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
* Check your...
    * Slack threads
    * DMs
    * Mentions
    * Email
    * Google Docs activity
    * Figma activity
    * Browser history
    * Calendar
    * Issue boards
    * Commit history
    * Code reviews
```

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2023/04.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2023/06.md)

# Work Log

<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->

# WEEK ENDING FRIDAY 2023-05-26

##### Meetings

* 4hr

##### Work

* Backlog cleanup
* PM catchup
* File roadmap issues for starter kit
* Replace the signup flow
* Hubspot for signup flow
* New page template for signup flow
* Updated navigation for signup flow
* Redirects for signup flow
* Reduced hours for cat blindness & dying
* Investigate pagespeed improvements for TractorSupply
* Writesonic admin support for Docs team

# WEEK ENDING FRIDAY 2023-05-19

##### Meetings

* 2hr

##### Work

* Emergency Cloudinary outage
* Draft a business case for design team hiring
* Review landing page template
* eCommerce demo
* Added new section to blog posts
* Implemented support for blog post series
* Reduced hours for cat blindness & dying
* Help with US based QA

# WEEK ENDING FRIDAY 2023-05-12

##### Meetings

* 2hr

##### Work

* New template for case studies
* eCommerce demo
* Presentation for AI LLM GPT research
* Updated links to community Slack on Docs and Web
* Reduced hours for cat blindness & dying

# WEEK ENDING FRIDAY 2023-05-05

##### Meetings

* 1hr

##### Work

* AI LLM GPT research
* Typeform integration component
* Use GPT to refactor document schemas with reusable fields
* Bugfix for CtaForm buttons
* Adjustments to Hero and CtaForm components

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2023/04.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2023/06.md)

# Weekly Template

```
# WEEK ENDING FRIDAY 2024-00-00

##### PTO

* #h

##### Meetings

* #h

##### Work

* Things I planned to do this week.
* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
* Check your...
    * Slack threads
    * DMs
    * Mentions
    * Email
    * Google Docs activity
    * Figma activity
    * Browser history
    * Calendar
    * Issue boards
    * Commit history
    * Code reviews
```

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2024/09.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2024/11.md)


# Work Log

<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->

# WEEK ENDING FRIDAY 2024-10-25

##### Work

* Blog post related content section
* LP for giveaway
* Content updates
* Analytics reports
* Weekly SEO

# WEEK ENDING FRIDAY 2024-10-18

##### Work

* Content segmentation in HockeyStack
* Update header styles for consistency
* SEO deprecated content cleanup
* Content updates
* Analytics reports
* Weekly SEO

# WEEK ENDING FRIDAY 2024-10-11

##### Work

* Pricing page
* Update case study template to allow hiding empty fields
* Add Windows ARM build to download page
* Notion dash for current quarter status
* Content updates
* Analytics reports
* Weekly SEO

# WEEK ENDING FRIDAY 2024-10-04

##### Work

* Company offsite

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2024/09.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2024/11.md)

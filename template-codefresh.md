# Boards

### [Growth Marketing Board](https://codefresh-io.atlassian.net/jira/software/projects/GROW/boards/98)

[Carmelle Board](https://trello.com/b/C2PF78hC/carmelle) || [Marketing Board](https://trello.com/b/3IzMdZ5m/marketing)

# Quick links

[Marketing OKRs](https://docs.google.com/spreadsheets/d/13eOMLHlfWxYuvPAxcsb2l1I5Los7lcXUviK-130yiwI/edit?usp=sharing) || [Growth Marketing Google Drive](https://drive.google.com/drive/folders/0AEyIZpW8hdhMUk9PVA) || [Marketing Google Drive](https://drive.google.com/drive/folders/1gXcV1l7wesSxuRpfS2abi_RcKhUSp99x)

# Templates

[Request a project](https://gitlab.com/brandon_lyon/documentation/-/blob/master/templates/request-a-project.md) || [Bug report](https://gitlab.com/brandon_lyon/documentation/-/blob/master/templates/website-bug-report.md)

# Checklists

[Merge request](https://gitlab.com/brandon_lyon/documentation/-/blob/master/checklists/merge-request.md) || [DIB](https://gitlab.com/brandon_lyon/documentation/-/blob/master/checklists/diversity-inclusion-belonging.md) || [Inherited site](https://gitlab.com/brandon_lyon/documentation/-/blob/master/checklists/audit-an-inherited-software-project.md) || [AB test](https://gitlab.com/brandon_lyon/documentation/-/blob/master/checklists/ab-test.md) || [Redirect](https://gitlab.com/brandon_lyon/documentation/-/blob/master/checklists/redirects.md)

# Guides

[Image guidelines](https://gitlab.com/brandon_lyon/documentation/-/blob/master/docs/image-guidelines.md) || [Prepared statements](https://gitlab.com/brandon_lyon/documentation/-/blob/master/docs/prepared-statements.md) || [FAQ](https://gitlab.com/brandon_lyon/documentation/-/blob/master/docs/faq.md)

# Weekly template

```
# WEEK ENDING MONDAY 2022-00-00

##### Overtime

* Note if there has been any overtime

##### PTO

* Note if the week is less than 5 days

##### Meetings

* Number of hours impacted by meetings

##### Work

* Things I planned to do this week.
* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
* Check your...
    * Slack threads
    * DMs
    * Mentions
    * Email
    * Google Docs activity
    * Figma activity
    * Browser history
    * Calendar
    * Issue boards
    * Commit history
    * Code reviews
```

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2021/05.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2021/07.md)


# Work Log

<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->

# WEEK ENDING 2021-00-00

##### PTO

* Note if the week is less than 5 days

##### Meetings

* Number of hours impacted by meetings

##### Work

* Things I planned to do this week.
* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
* Check your slack threads, DMs, browser history, and gitlab activity.

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2021/05.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2021/07.md)

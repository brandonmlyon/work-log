# Weekly Template

```
# WEEK ENDING THURSDAY 2022-06-00

##### Overtime

* Note if there has been any overtime

##### PTO

* Note if the week is less than 5 days

##### Meetings

* Number of hours impacted by meetings

##### Work

* Things I planned to do this week.
* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
* Check your...
    * Slack threads
    * DMs
    * Mentions
    * Email
    * Google Docs activity
    * Figma activity
    * Browser history
    * Calendar
    * Issue boards
    * Commit history
    * Code reviews
```

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2022/09.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2022/11.md)


# Work Log

<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->

# WEEK ENDING THURSDAY 2022-10-27

##### Meetings

* 1hrs

##### Work

* Added SEO, pageview, and components needed to spreadsheet
* Implemented Cloudinary
* Component fallback options
* Toggle for fullsize images
* Tagmanager
* CMS deployment strategy
* CMS staging environment
* Import 11 blog posts
* Image attachment styles
* Implement tech for blog categories
* Author schema
* Form schema and component

# WEEK ENDING THURSDAY 2022-10-20

##### Meetings

* 4hrs

##### Work

* Wrapper component
* Bug - icon card wrapping
* Bug - CTA button wrapping
* WYSIWIG styles
* Markdown component
* Blockquote styles
* New content type subfolder - product
* 5 other content types
* Adapted 5 pages from old site

# WEEK ENDING THURSDAY 2022-10-13

##### Overtime

* 2 hours

##### Meetings

* 3 hours

##### Work

* Component - card grid
* Code cleanup
* Footer
* PoC demo
* Top nav phase 2
* Spreadsheet of old content

# WEEK ENDING THURSDAY 2022-10-06

##### Meetings

* 2 hours

##### Work

* Baseline CSS
* Component - hero default
* Component - hero variants
* Default meta and Open Graph copy and images
* Import Chakra from product
* Top nav phase 1
* Component - side by side
* Component - CTA
* Favicon
* DNS infrastructure

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2022/09.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2022/11.md)

# Weekly Template

```
# WEEK ENDING MONDAY 2022-06-00

##### Overtime

* Note if there has been any overtime

##### PTO

* Note if the week is less than 5 days

##### Meetings

* Number of hours impacted by meetings

##### Work

* Things I planned to do this week.
* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
* Check your...
    * Slack threads
    * DMs
    * Mentions
    * Email
    * Google Docs activity
    * Figma activity
    * Browser history
    * Calendar
    * Issue boards
    * Commit history
    * Code reviews
```

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2022/08.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2022/10.md)


# Work Log

<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->

# WEEK ENDING MONDAY 2022-09-29

##### Meetings

* 4hrs

##### Work

* Data model
* CSS framework
* JS framework
* SSG
* Documentation

# WEEK ENDING MONDAY 2022-09-22

##### Meetings

* 4hrs

##### Work

* Proposal for new website
* Roadmap for new website
* Kickoff for new website
* Video kickoff
* Start PoC for new website
* Documentation for new tech

# WEEK ENDING MONDAY 2022-09-15

##### Work

* Onboarding
* Implement Jira workspace

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2022/08.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2022/10.md)

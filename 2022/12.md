# Weekly Template

```
# WEEK ENDING THURSDAY 2022-06-00

##### Overtime

* Note if there has been any overtime

##### PTO

* Note if the week is less than 5 days

##### Meetings

* Number of hours impacted by meetings

##### Work

* Things I planned to do this week.
* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
* Check your...
    * Slack threads
    * DMs
    * Mentions
    * Email
    * Google Docs activity
    * Figma activity
    * Browser history
    * Calendar
    * Issue boards
    * Commit history
    * Code reviews
```

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2022/11.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2023/01.md)


# Work Log

<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->

# WEEK ENDING THURSDAY 2022-12-29

##### PTO

* 24 hours

##### Work

* Documentation video outlines
* Major version update for CMS systems
* Update node packages
* Final QA pass
* Strategy for preview environments
* Roadmap updates

# WEEK ENDING THURSDAY 2022-12-22

##### Overtime

* 12 hours

##### Meetings

* 1hr

##### Work

* Footer duplicate form bugfix
* Skeleton loader for forms
* Favicon fix
* Implement final pages
* Announce final milestone
* Record a video preview

# WEEK ENDING THURSDAY 2022-12-15

##### Meetings

* TBD

##### Work

* Homepage
* Footer phase 2
* Mobile font sizes
* Remaining pages audit

# WEEK ENDING THURSDAY 2022-12-15

##### Overtime

* 8 hours

##### Meetings

* 1 hrs

##### Work

* Page speed audit - major headache
* Content migration 181-200
* Enterprise page
* Cloudflare page
* Pricing page
* Code review
* Open graph image audit
* Cookie audit

# WEEK ENDING THURSDAY 2022-12-08

##### Meetings

* 1 hrs

##### Work

* Document do's and don'ts
* Content migration 111-180
* Dev week page

# WEEK ENDING THURSDAY 2022-12-01

##### Meetings

* 0 hrs

##### Work

* Table of Contents list for series
* Bug cleanup for series routing, component displays, fallbacks, and options
* Content migration 51-110

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2022/11.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2023/01.md)

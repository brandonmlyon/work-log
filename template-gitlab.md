# Boards

### [DE Kanban](https://gitlab.com/groups/gitlab-com/-/boards/2415063) || [DE Sprint](https://gitlab.com/groups/gitlab-com/-/boards/2415116)

[Sprint New](https://gitlab.com/groups/gitlab-com/-/boards/1761580?scope=all&utf8=%E2%9C%93&label_name[]=mktg-inbound&not[label_name][]=mktg-status%3A%3Atriage&not[label_name][]=mktg-status%3A%3Ablocked) || [Sprint Classic](https://gitlab.com/groups/gitlab-com/-/boards/1483370?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-website&not[label_name][]=mktg-status%3A%3Atriage&label_name[]=mktg-inbound&assignee_username=brandon_lyon) || [Inbound leaders](https://gitlab.com/groups/gitlab-com/-/boards/2371969) || [DS sprint](https://gitlab.com/groups/gitlab-com/-/boards/2248456?label_name[]=mktg-website%3A%3Adesign-system&label_name[]=mktg-inbound&label_name[]=mktg-website)

# Sticky tasks

[OKRs](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/318) || [AB test history](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/338) || [Localization Q1](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/271) || [Localization Q2](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/322) || [Slippers](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/218) || [CMS](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/192) || [Commit template](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/275) || [Cookiebot](https://gitlab.com/groups/gitlab-com/-/epics/681) || [Marketo](https://gitlab.com/groups/gitlab-com/-/epics/699) || [www DRI](https://gitlab.com/gitlab-com/Product/-/issues/1869) || [Lost dashboards](https://gitlab.com/gitlab-com/business-technology/team-member-enablement/issue-tracker/-/issues/1522#note_549825069). || [Metrics](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/308) || [Capacity math](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1243)

# Quick links

[Example epic](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/192) || [Example issue](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/546) || [Page update](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-update-webpage-brief) || [New page](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-new-website-brief) || [Other](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-website-other) || [Bug report](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=-website-bug-report)

# Daily template

```
# DAY 2021-00-00

##### Meetings

* List.

##### Work

* Things I planned to do today.
* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
* Check your slack threads, DMs, browser history, and gitlab activity.
```

# [Previous month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2021/01.md) || [Next month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2021/03.md)

<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->

# DAY 2021-00-00

# [Previous month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2021/01.md) || [Next month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/03.md)

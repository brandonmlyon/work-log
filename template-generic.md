# Weekly Template

```
# WEEK ENDING FRIDAY 2025-00-00

##### PTO

* Note if the week is less than 5 days

##### Meetings

* Number of hours impacted by meetings

##### Work

* Things I planned to do this week.
* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
* Check your...
    * Slack threads
    * DMs
    * Mentions
    * Email
    * Google Docs activity
    * Figma activity
    * Browser history
    * Calendar
    * Issue boards
    * Commit history
    * Code reviews
```

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2025/01.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2025/03.md)


# Work Log

<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->

Lorem ipsum dolor sit amet...

# [Previous month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2024/01.md) || [Next month](https://gitlab.com/brandonmlyon/daily-log/-/blob/main/2025/03.md)

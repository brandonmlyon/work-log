# Boards, links, & notes

* [BML Attention](https://bit.ly/2yqOt8h).
* [Sprint](https://gitlab.com/groups/gitlab-com/-/boards/1483370?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-growth&label_name[]=mktg-website&not[label_name][]=mktg-status%3A%3Atriage&assignee_username=brandon_lyon).
* `@gl-about-growth`

# Sticky tasks

* [Workflow issues](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/23).
* [Cookiebot improvements](https://gitlab.com/groups/gitlab-com/-/epics/681).
* [Marketo improvements](https://gitlab.com/groups/gitlab-com/-/epics/699).
* [Marketo page template](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/43).
* [Vendor onboarding](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/98).
* [Homepage content](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/-/issues/99) & [Homepage swap](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/22).
* [Event page updates](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/18).

# Daily template

<details>
<summary>show/hide</summary>

```

# DAY 2020-00-00

##### Notes.

* List.

##### Meetings

* List.

##### Asides

* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.

##### Primary tasks

* Things I planned to do today.

```

</details>

# [Previous month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/05.md) || [Next month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/07.md)

# TUES 2020-06-30

##### Notes.

##### Meetings

* Weekly 1:1.

##### Asides

* Long slack discussions re sprint problems.
* Fielded more support requests for cookiebot.
* [One of the support requests finally lead to an actionable bugfix](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8052).

##### Primary tasks

* Pivoted to ^
* Touched base with contractors.

# MON 2020-06-29

##### Notes.

* Worked extra hours.

##### Meetings

* Weekly sprint planning.

##### Asides

* Fielded slack feedback re UX of blocked resource message.
* Slack legal discussion re icons.
* Assist Lauren with MR/issue procedures re while she was out.
* Created @gl-about-growth group.
* Slack discussion re conversion design of buttons.
* Issue refinement & spring planning.
* [Filed issues for Marketo forms](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8118).
* Educate re event requests and timeline impacts etc.
* [Commit add sponsors](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/111).

##### Primary tasks

* Pivoted to ^

# FRI 2020-06-26

##### Notes.

* Taking third of the day off because I've already put in > 40 hrs this week.

##### Asides

* Assist Jarek w/ cookiebot review app video embed & record dev workaround.
* [Field question re templates and canonical](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/125).
* Created epic for tracking Marketo issues.

##### Primary tasks

* [Finished up GitOps topic page](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7798)
* [Speakers section for valuedriver layouts](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/92).
* [Update homepage](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/121).

# THURS 2020-06-25

##### Notes.

* Started an hour early today.
* Didn't get to eat lunch.
* Worked 3 extra hours total.

##### Meetings

* Weekly recap.
* Weekly marketing.
* B&D biweekly.

##### Asides

* [Small MR assist](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/54104).
* [Assist Javi with HashiCorp solutions page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/54104).
* CultureAmp survey.
* Chats re Commit page.
* [Assist with MR related to Commit page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/54172).
* [Research refresh loop](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8052).
* Consult re Bizible.
* [Assist in slack with handbook oncall re codeowners](https://gitlab.slack.com/archives/CVDP3HG5V/p1593103069296400).
 
##### Primary tasks

* [GitOps topic page](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7798)

# WED 2020-06-24

##### Meetings

* Lauren 1:1.
* Cookiebot sync with Sarah.

##### Asides

* Marketo form wrangling via Slack.
* Issue refinement from WIP into Triage.
* Advice on adding video to issue templates via Slack.
* [Event sponsors issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/111).
* Bizible slack discussion.
 
##### Primary tasks

* Vendor issue refinement.
    * I finally made progress, huzzah!
* [Brittany issue with Tipalti](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/187).

# TUES 2020-06-23

##### Notes.

* Time lost this week: 1 day due to traveling due to fire.

##### Meetings

* Weekly 1:1 Michael.

##### Asides

* [Cookiebot epic created, related issues filed, sync sessions scheduled, and had slack conversations](https://gitlab.com/groups/gitlab-com/-/epics/681).
* Vendor issue refinement.
* Lots of issue refinement.
 
##### Primary tasks

* Nothing major planned due to fire in Paso Robles.

# MON 2020-06-22

##### Meetings

* Sprint planning.

##### Asides

* Review merge requests.
* [Vendor: Gitlab guide to cloud](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7754).
* [Bug on the trial banner](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8046).
* [Blog related posts](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/2974).
* [Clean up Demo experiment](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/53716)
* [Preping doc to welcome back Lauren](https://docs.google.com/document/d/1_ysDsjoeM_l9OeT6UTOP3QspLX54fX6us2EwfO2gjjE/edit)
 
##### Primary tasks

* [Add Eventbrite to Commit landing page](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/109).

# FRI 2020-06-19

##### Meetings

* Weekly recap.

##### Asides

* DAG feature feedback.
* Analytics of pricing page segments.
* Advice regarding computer monitors & ergonomics.
* Slack discussion re benefits.
* Drop in trials attribution.
* [Speakers section for campaign landing page](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/92).
* [Commit SF recap](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7094).
* Event page epic.
* Slack discussions re homepage hero cycles.
 
##### Primary tasks

* Marketo placeholder copy.

# THURS 2020-06-18

##### Meetings

* Weekly marketing.
* Weekly 1:1.
* Digital design weekly.

##### Asides

* [Bigfix for pricing page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/53378).
* [Trademark page moved](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/53385).
* Slack conversation re blog bug.
* Slack followup re vendor showcasing us as a customer.
* Slack followup Hompeage re analytics for scheduled content & what's scheduled.
* Slack conversation re review app lifespan.
* Feedback on issue re laptop management.
* Review codeowners plan.
* [Review localization vendor](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/2242).
* Great place to work survey.
 
##### Primary tasks

* Add cookiebot warning to the education page.
* Vendor onboarding?
    * Refine issues for them to work on & assign.

# WED 2020-06-17

##### Notes.

* Worked 6 hours during VTO.

##### Meetings

* Brittany onboarding.

##### Asides

* [Review localization vendor](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/2242#note_363210462).
* [Documentation for reviewing merge requests](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/52991).
* [Upload filesize limit Slack conversation](https://gitlab.slack.com/archives/CN8AVSFEY/p1592419748159900).
* Slack discussion w/Becky re old www-gitlab-com issue templates.
* Slack discussion w/Stephen re vendor error.
* [Research re paying vendors](https://about.gitlab.com/handbook/finance/procure-to-pay/).
* [Slack discussion re deep link anchors](https://gitlab.slack.com/archives/CN8AVSFEY/p1592346782155100).
* [Advise Nuritzi re UX of partners page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/52882).
* Email & issue refinement.
* [Slack discussion re review app lifespan](https://gitlab.slack.com/archives/CVDP3HG5V/p1592220778235100).
* [Onboarding Michael via merge request for badge css](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/50080).
* [Comment on ops issue re Marketo form fallbacks](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/526).
 
##### Primary tasks

* [Cookiebot refresh after accept](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7845).
* Brittany onboarding.
* [Pricing page AB test](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/52596).

# TUES 2020-06-16

* VTO

# MON 2020-06-15

* VTO

# FRI 2020-06-12

* Company holiday

# THURS 2020-06-11

##### Meetings

* Weekly marketing.
* B&D biweekly.
* Coffee chat with Michael.
* Weekly recap.

##### Asides

* [Assist with MR conflicts](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/51855).
* Refine incoming issues for [events yaml](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/51855), partner marketing's [HashiCorp](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/90) and [Red Hat](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/91).
* [Review handbook MR for design documentation](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/52739).
* [Flesh out blog search issue for outsourcing](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6649).
* [Pricing page add link to FAQ](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/34).
* Fielded slack request for location to edit of features.yml ldap group sync filters via Martin Brummer.
 
##### Primary tasks

* Backlog refinement.
    * Setup next few weeks.
* [Vendor onboarding](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/98).
    * Organize & group the vendor tasks.

# WED 2020-06-10

##### Notes.

* After hours last night I ran point for an issue with our search service provider.

##### Meetings

* Weekly 1:1.
* Pair programming with ShaneR re pricing AB test.

##### Asides

* [Advise re handbook elements](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/52431).
* [LaunchDarkly finance issue](https://gitlab.com/gitlab-com/finance/-/issues/2381).
* [AR and learning ContractWorks](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/5624).
* Email & issue refinement.
* Troubleshoot CDN Issue.
* Assist Monica in Slack re zoom background.
* Assist digital marketing in Slack re handbook document link.
* [Whitelist-blacklist documentation](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/51812).
 
##### Primary tasks

* [Pricing page test setup](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/132).

# TUES 2020-06-09

##### Notes.

* List.

##### Meetings

* Javier onboarding.

##### Asides

* [Cookiebot & GDPR UX](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/brand-and-digital/-/epics/9).
* [Assist with relocating issue template](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/merge_requests/12).
* [Make case studies searchable](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6082).
* [Webcast template updates](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/40584).
* [Whitelist-blacklist documentation](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/51812).
* [Search provider outage](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7959).
 
##### Primary tasks

* [Marketo page template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7259).
    * Update placeholder copy.
    * Finalize & close out issue.

# MON 2020-06-08

##### Notes

* Worked 30 min late

##### Meetings

* Sprint planning.

##### Asides

* (2hr) Issue refinement.
* [Code review for Stephen Gitlab.tv MVC](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/52009).
* (1.5hr) [Create the event landing page template epic](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/18).
* Marketo page template mobile bugfix: truncate trust list on mobile.
* Monorepo refactor blog pipeline bug review.
* (1hr) [Homepage hero popup to link](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/52104).
* (.5hr) UX feedback for product toasts.
 
##### Primary tasks

* [New version control page](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6598).
* Vendor onboarding - see personal notes file.
* Price test assistance.

# FRI 2020-06-05

##### Meetings

* Sprint planning.
* Weekly recap.

##### Asides

* [YAML linting](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/47628/).
* Issue refinement emails.
* Marketo page template as seen on mobile.
* [AR for ContractWorks per LD vendor sourcing issue](https://gitlab.com/gitlab-com/finance/-/issues/2381).
 
##### Primary tasks

* [New version control page](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6598).
* Vendor onboarding - see personal notes file.

# THURS 2020-06-04

##### Meetings

* Weekly marketing tactics.
* Weekly digital design.

##### Asides

* [Add opengraph image to hero templates](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/51879).
* [Handbook markdown TOC processing](https://gitlab.com/gitlab-org/gitlab/-/issues/220340#note_355751306).
 
##### Primary tasks

* [Add video to commit landing page](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7788).
* [Marketo page template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7259).
    * Update default org logos.
    * Update default library graphic.
    * Update docs.

# WED 2020-06-03

##### Asides

* [Monorepo approach](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7898).
* [LaunchDarkly finance](https://gitlab.com/gitlab-com/finance/-/issues/2381).
* Email conversation with LaunchDarkly.
* [Code review for product growth pricing](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/51390).
* [VCS page](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/87).
* [Bugfix for TinaS partners logos](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/50754).
* (>1hr) [Homepage hero strategy](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/67).
* [Handbook page mapping slack discussion](https://gitlab.slack.com/archives/C0AR2KW4B/p1591131215021300).
 
##### Primary tasks

* No time.

# TUES 2020-06-02

##### Meetings

* Sprint planning pt2.
* Weekly 1:1.

##### Asides

* (45min) [Code review and backend education with ChadW](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/51499).
* [Event landing page template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6295).
 
##### Primary tasks

* [Homepage hero swap](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/68).
* [Vendor onboarding](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/98).

# MON 2020-06-01

##### Notes

* This entire day was taken up by emails re issue comments and refinement plus two short meetings.

##### Meetings

* [Sprint planning](https://gitlab.com/groups/gitlab-com/-/boards/1483370?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-website&assignee_username=brandon_lyon).
* Pricing page testing
    * [Meeting agenda](https://docs.google.com/document/d/141RY2ozQDK5qmzfXkTl6Kq95hpdbRkpRXgAq6i-rjec/edit).
    * [Resulting issue](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/11).

##### Asides

* Pricing page testing.
    * Chat with ShaneR re MR for copy updates.
* [Bugfix on developer survey](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7879).
* [German CI campaign](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/51559).
* Slack handbook escalation re review apps.
* [Code review](https://gitlab.slack.com/archives/C81PT2ALD/p1591043392215200) of [Handbook update](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/51499) with ChadW.
* [Discussion re homepage hero weekly swap](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/67#note_353224056).
* Issue refinement
    * [Blog page more info](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/55).
    * [Sort/filter options for release page](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7884).
    * [Vendor stuff](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6295).
* [All remote sidebar](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/2690).
* 360 peer reviews.
 
##### Primary tasks

* (30 min multitasking during a meeting) [Cookiebot translation](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/86).

# [Previous month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/05.md) || [Next month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/07.md)

# [My sprint](https://gitlab.com/groups/gitlab-com/-/boards/1483370?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-growth&label_name[]=mktg-website&not[label_name][]=mktg-status%3A%3Atriage&assignee_username=brandon_lyon).

# Quick links

[New branch](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new) || [Request update](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-update-webpage-brief) || [Request new page](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-new-website-brief) || [Request other](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-website-other) || [Bug report](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=-website-bug-report) || `@gl-about-growth`

# Sticky tasks

[Before we can CMS](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/71) || [CMS](https://gitlab.com/groups/gitlab-com/-/epics/705) || [Cookiebot improvements](https://gitlab.com/groups/gitlab-com/-/epics/681) || [Marketo improvements](https://gitlab.com/groups/gitlab-com/-/epics/699) || [Event page updates](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/18) || [CMS requirements](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/223) || [Refresh stragegy](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/32) || [Top 20 refresh](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/23) || [Enterprise phase 4](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/295) || [Navigation revamp](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/86) || [Blog redesign](https://gitlab.com/groups/gitlab-com/-/epics/158)

# Daily template

```
# DAY 2020-00-00

##### Notes.
* List.

##### Meetings
* List.

##### Asides
* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
* Check your slack history, browser history, and gitlab activity.

##### Primary tasks
* Things I planned to do today.
```

# [Previous month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/07.md) || [Next month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/09.md)

<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->

# MON 2020-08-31

##### Meetings

* Cookiebot meeting.
* Sprint planning.

##### Asides

* [Inform SSEG about handbook update for all-remote CTA](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/2690).
* Slack re trial dropoffs.
* Slack re pricing A/B test header bug.
* Slack re Topic pages epic vs Topic articles epic.
* Minor handbook documentation updates.

##### Primary tasks

* Prep for cookiebot meeting.
* Sprint planning.
* Issue refinement.
* [File epic for topic articles](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/98).
* Monthly recap.

# FRI 2020-08-28

##### Notes.

* Not a great day. I was definitely a red checkin at the end.

##### Asides

* Assists with MR such as [link](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/60525)
* [UX of blocked resource notice](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/95).
* Slack re pricing project retrospective.

##### Primary tasks

* [Pricing page update](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/279).
* [Homepage update](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/332).

# Thurs 2020-08-27

##### Notes.

* Started work early for a total of 2 hours extra.

##### Meetings

* Weekly recap.
* Lauren 1:1.
* Topic pages kickoff.

##### Asides

* [Smartling integration](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/314).
* [Filed issue for pricing test followup](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/356).
* [Kickoff for topic articles](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/64).
* [Homepage hero swap](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/332/).
* [Homepage bugfix](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/60963).
* [Decouple handbook from marketing site](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8925).
* [MR reviews and assists](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/60954).
* [Split design system into multiple issues](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/334).
* Slack re SSE MR changes.
* Slack re about.gitlab purchase workflow.
* Slack re pricing followup.
* [Several hours splitting up and pointing the "Before we can CMS" project](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/71).
* Slack re merge trains.
* Slack re vendor updates re monorepo.
* Quickie review of the CMS proof of concepts.
* Slack followup re tracking of free trial CTA on homepage.
* Troubleshoot pipeline errors.
* Slack re question for growth group re pricing page.
* Slack review SSEG code update re pipeline error checks.

##### Primary tasks

* [Commit](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/60834).
* [Pricing udpates](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/279).

# Wed 2020-08-26

##### Notes.

* Started work early for a total of 3 hours extra for Commit.

##### Asides

* Slack re commit.
* Slack re handbook footer edit in web-ide buttons.
* Slack re Moqups tool ownership.
* [Handbook documentation re scheduled releases](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/#can-i-request-a-release-time).

##### Primary tasks

* Commit homepage takeover.
* [Free trial homepage takeover](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/332).
* [Commit post even updates](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/304).

# TUES 2020-08-25

##### Notes.

* Worked an extra hour.

##### Asides

* [Adjust to final release of monorepo rollout](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8643).
* [Homepage hero takeover pivot for free trials](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/332).
* [Fix bug with pricing test metrics](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8893).
* [Slack and issues re milestone bug](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8643#note_401849238).
* [Pivoted to update the timeframe for the Commit page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/60711).
* [Pivoted again to the same thing](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/60737).
* Slack re Marketo templates.
* [Slack re other teams pushing people into Bronze wheen we're trying to push people out of Bronze](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/3367).
* [Slack re SSEG workflow review](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/60718#note_402013062).
* Slack re Tanuki mascot name (Ruby).
* Investigate LaunchDarkly quota allotment for pricing test.
* Slack CEO chat re clarifying test process and results.
* [Slack re Total Rewards and locale metro](https://gitlab.slack.com/archives/CTVK60M32/p1598312153018500).
* [Slack re SSEG moving towards block editors](https://gitlab.slack.com/archives/CQ29L6B7T/p1598344070035000).
* [Review issues re image CDN and external pipelines](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/5323).
* [Groom issues re pricing plans](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/279).
* Slack re Marketo-Cookiebot issues.
* [Slack re updating the Drupal sponsor logo](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/60637).

```
Q: If someone in EMEA comes to our website, and refuses cookies - do they still see our Marketo Form? or they are prevented from seeing it?
A: They would be prevented from seeing it. Currently we show them a notice after refusing which says they need to allow to use the form.
Q: Can we adjust it so that they would still see the form even if they refuse cookies? or does something prevent that?
A: We have some issues filed to investigate if it's possible to use the form without Marketo setting cookies but have not been able to prioritize that.
https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/214
That is the one we would try first but there are a couple of other options should it fail,
https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/256
https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8583
It's legal/policy related and Marketo related. We could exempt them from Cookiebot if Marketo didn't set cookies.
Q: It sounds like Todd would prefer if we still show the form even if they don’t accept cookies.
A: It would be preferable to show the form but we're not sure if it's possible at the moment. It sounds like it is possible but currently other projects are prioritized ahead of it like the Commit event, pricing updates, enterprise page, free trials, and CMS.
```

##### Primary tasks

* [Commit homepage takeover](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/60694).

# MON 2020-08-24

##### Meetings

* Weekly planning.

##### Asides

* Slack re milestone bug.
* Updated bug report template.
* [Bug report for mobile menu on event template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8882).
* [Commit homepage takeover](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/127).
* [Commit free trial CTAs](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/326).
* [Breakdown and estimate issues for the epic "Before we can CMS"](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/327).

##### Primary tasks

* [Update commit speakers](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8880).

# FRI 2020-08-21

##### Notes.

* Worked an extra 3 hours.

##### Asides

* Bugfix in pricing page metrics selectors.
* Slack re resellers partners.
* Slack re vendors.
* Pipeline troubleshooting.
* MR reviews & assists.
* [CMS security requirements](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/241#note_400243470).
* Several offboarding issues throughout the week.
* Handbook reading/research.
* Slack re visual editor for gitlab-config file.

##### Primary tasks

* [Pricing test](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/60300).

# THURS 2020-08-20

##### Meetings

* Marketing weekly.
* Weekly recap.

##### Asides

* Slack re branch naming conventions.
* Slack re publishing workflow prototype.
* Issue refinement.
* Slack re Pathfactory.
* Slack re Sched embed.
* [Assist with MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/57716#c730845e1cd8bb9ba219f5a330e29b1985270914).
* Slack re vendor assistance.
* Updating the "Before we can CMS" epic.

##### Primary tasks

* [Pricing test](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/60300).

# WED 2020-08-19

##### Meetings

* ShaneB goodbye.
* Pricing page sync.
* Lauren sync.
* Enterprise refresh kickoff.

##### Asides

* Surprisingly few if any.

##### Primary tasks

* [Pricing test](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/60300).

# TUES 2020-08-18

##### Notes.

* [Pivot to pricing test](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/212#note_394360884).
  * Need to file an issue for this iteration.

##### Meetings

* Sync on covering for Michael.

##### Asides

* [File issue for CMS budget estimate](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/307).
* [File AR issue for Dovetail](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/6493).
* [Assist with structured data for events page](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/270).
* Slack re Marketo landing page troubleshooting of speakers list.
* Slack re SSEG tools and pipelines.
* Field pipeline troubleshooting requests.
* Translate various [games of telephone](https://en.wikipedia.org/wiki/Chinese_whispers).

##### Primary tasks

* Expense report.
* CMS price estimate.
* Pricing test.

# MON 2020-08-17

##### Notes.

* PTO.

# FRI 2020-08-14

##### Notes.

* Company time off, Friends & Family day.

# THURS 2020-08-13

##### Notes.

> We’ve enabled the “Nominator” bot on Slack. This can be used to nominate someone for a discretionary bonus. To do this, you type /nominate Name Of The Person. A modal should pop-up that asks you about the details. You can type /nominate help for more.

##### Meetings
* Weekly recap.

##### Asides
* [Vendor spreadsheet update](https://docs.google.com/spreadsheets/d/1Q36dnr60R6PT8CTu0mvoBUaV80ECTb6cheWvOcaQCWo/edit#gid=0).
* [Pricing updates issue refinement](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/279#note_395659346).
* Assist with 4 MRs.
* Slack re middleman.
* [MR to update end date for commit scholarships](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/59645/).
* Help Javi with blog search.

##### Primary tasks
* N/A

# WED 2020-08-12

##### Asides
* Slack re blog post Pathfactory templates.
* Slack re logo use on enterprise page.
* Slack re URLs ending with index.html.
* Slack vendor support.
* Slack re CDN providers with Dara.
* Slack re comparison table on homepage.
* Slack re fielding bug reports.
* Help Javi with blog search.

##### Primary tasks
* [Event page video band](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/123).
* [Groundworks branding](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/120).
* [Cusomters filters for EDU and OSS](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/278).

# TUES 2020-00-00

##### Asides
* Slack re vendor access to www-gitlab-com.
* Slack re comparison chart on homepage.
* [Design system sync with SSEG](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/32#note_394169771).
* [Bug report re branch naming](https://gitlab.com/gitlab-org/gitlab/-/issues/235512).
* [Commit event bug 1](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8724).
* [Commit event bug 2](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/295).

##### Primary tasks
* [Add blocked resource warning to services pages](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/272).
* [Create tags for OSS and EDU on customers page](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/278).
* [Update groundworks branding](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/120).

# MON 2020-08-10

##### Meetings
* Sprint planning.

##### Asides
* Slack re SSEG Pajamas HAML bootstrap.
* Slack re vendors.
* [Slack re comparison pages](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/13).
* Slack re pubsec commit page.
* Planning re pricing page future updates.
* [Deprecate old enterprise related pages](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/59099).
* [Plan ROI calculators](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/252).
* [Create MR template for growth marketing](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/59219).
* [MR assisting Tim Rizzi with popup](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/57934).
* [Vendor MR review](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/59281/).

##### Primary tasks
* [Commit event sponsor logos](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/164).

# FRI 2020-08-07

##### Notes.

* Worked 1 hour late.

##### Asides

* Slack re customers page.
* Slack re pipeline UX.
* Assist vendors.
* Slack re pricing page.
* Slack re enterprise page.
* Review monorepo epic current status.
* Slack re SSOT and epic nesting.
* Disable the pricing page test.

##### Primary tasks

* Commit page updates.

# THURS 2020-08-06

##### Meetings

* Weekly marketing.
* Q3 web projects kickoff.
* B&D biweekly.
* Weekly recap.

##### Asides

* [Updated documentation for image requirements](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/58958).
* [Updated issue templates](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/blob/master/.gitlab/issue_templates/request-website-other.md).
* MR reviews.
* [URL rewrites](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/5706).
* Updated personal log formatting and strategy.

##### Primary tasks

* [Created issue for design system MVC](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/284).

# WED 2020-08-05

##### Meetings

* Merge request shadow/training.
* CMO skip level.
* Lauren coffee chat.

##### Asides

* [Reported infrastructure trouble](https://gitlab.com/gitlab-org/gitlab/-/issues/233838).
* [Groundworks minor updates](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/281).
* [Document new AB test parameters](#).

##### Primary tasks

* [Pricing page iteration](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8584).

# TUES 2020-08-04

##### Meetings

* ShaneB leaving announcement.

##### Asides

* [Create an issue for pricing page mobile optimization](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/276).
* Review issues to assign to Brittany.
* Slack questions for product growth re why pricing URL ID parameters vs a human-readable URL (ease of build).

##### Primary tasks

* [Pricing page iteration](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8584).

# MON 2020-08-03

##### Notes.

##### Meetings

* Sprint planning.

##### Asides

* Slack re serverless in the context of JAM stack.

##### Primary tasks

* Create pricing page issue.
* [Pricing page iteration](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8584).

# [Previous month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/07.md) || [Next month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/09.md)

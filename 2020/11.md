# [My sprint](https://gitlab.com/groups/gitlab-com/-/boards/1483370?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-inbound&label_name[]=mktg-website&not[label_name][]=mktg-status%3A%3Atriage&assignee_username=brandon_lyon&label_name[]=BML-attention).

# Quick links

[New branch](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new) || [Request update](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-update-webpage-brief) || [Request new page](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-new-website-brief) || [Request other](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-website-other) || [Bug report](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=-website-bug-report) || `@gl-about-growth`

# Sticky tasks

[CMS Roadmap](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/71) || [Design system](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/92) || [CMS](https://gitlab.com/groups/gitlab-com/-/epics/705) || [Cookiebot improvements](https://gitlab.com/groups/gitlab-com/-/epics/681) || [Marketo improvements](https://gitlab.com/groups/gitlab-com/-/epics/699) || [Event page updates](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/18) || [CMS requirements](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/223) || [Refresh stragegy](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/32) || [Top 20 refresh](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/23) || [Enterprise phase 4](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/295) || [Navigation revamp](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/86) || [Blog redesign](https://gitlab.com/groups/gitlab-com/-/epics/158) || [Pricing epic](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/78) || [Bolt on Netlify](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/622)

# Daily template

```
# DAY 2020-00-00

##### Notes
* List.

##### Meetings
* List.

##### Work
* Things I planned to do today.
* Things I did that were unplanned: sidetracked conversations, bugfixes, emergencies, etc.
* Check your slack history, browser history, and gitlab activity.
```

# [Previous month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/10.md) || [Next month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/12.md)

<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->
<!----------------------------->

# DAY 2020-11-30

##### Notes
* List.

##### Meetings

* CMS sprint kickoff.
* DS release candidate.
* CMS collaboration.

##### Work

* Issue refinement.
* [Cookiebot](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9954).
* [Cookie consent UX](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/412).
* Assist SAA.
* MR review for RD.
* [Connect with VB and investigate animation tools](https://createwithflow.com/).
* [MR assist BG with blog](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/67843).
* [Review MR for LB](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/69147).
* Slack re comparison pages.

# VTO 2020-11-25 to 11-27

# DAY 2020-11-24

##### Meetings

* Pair programming with BG.
* Weekly 1:1.

##### Work

* Update issue board to align with new labels for team rename.
* [Update documentation to align with new labels for team rename](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68964).
* [Assist SR with AB test MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68963).
* [Assist SAA with onboarding and CSS](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/566).
* [Assist SR with ROI calculator](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9990).
* [Review blog MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9985).
* [Slack re subpixel rendering and iconography](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1432).

> @jelder I wouldn't worry about my operating system subpixel aliasing question too much and would design for the majority rather than the edge cases. I haven't researched this area in a long time. The resources I'm finding at the moment are somewhat outdated. I was mostly curious since your issue seems knowledgeable and technical. There are also edge-cases for different subpixel display types (BGR vs RGB, RGBY, PenTile, etc) but since they're edge-cases I wouldn't recommend worrying about them. https://www.codeclouds.com/wp-content/uploads/2019/04/image4.gif - https://www.smashingmagazine.com/2012/04/a-closer-look-at-font-rendering/ - https://pandasauce.org/post/linux-fonts/ - https://blog.typekit.com/2010/10/15/type-rendering-operating-systems/

# DAY 2020-11-23

##### Notes

* The entire day was meetings, refinement, and management.

##### Meetings

* DS sprint planning
* CMS release candidate
* DS working group
* Homepage news componnent kickoff
* Sync with JG and SAA
* Sprint board creating with MP

##### Work

* [Create issue for homepage news engineering](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/708)
* [Update documentation for MR reviews](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/707).
* [Review MR for SR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68851)
* Review MR for LB
* [Review MR for BG](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68857)
* Slack re team org name changes and related MRs
* [Review MR for WC](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68758)
* [Review Figma mockup of homepage](https://www.figma.com/file/rf8lStJ2gmy3pa29wSt4UA/Home-Page-MVC-2?node-id=156%3A733)


# FRI 2020-11-20

##### Meetings

* CSS framework sync.

##### Work

* Investigate CSS frameworks.
* Slack and issue re free trial page navigation.
* [Assist with Marketo forms](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/66499).
* Slack re homepage feedback.
* [Assist with homepage MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68723).
* [Assist with security releases MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68327).

# THU 2020-11-19

##### Meetings

* Growth recap.
* Marketing weekly.
* CMS release candidate plan.
* Internal agency planning.
* DS sprint retro.
* Handbook DRIs.

##### Work

* [Assist LB with MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68327).
* [Cookiebot](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9954).
* Issue refinement.
* Slack re homepage and enterprise release feedback.
* [Review homepage feedback MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68574).
* Slack re design system.
* Slack re CSS frameworks.
* Create heatmaps for homepage and enterprise page.
* Slack re heatmaps and size of company vs testing limits.
* Slack re javascript on homepage.
* Slack re `window_title` frontmatter use by inbound marketing.
* Slack re handbook and auto linking URLs in markdown.

# WED 2020-11-18

##### Meetings

* DS create release video.

##### Work

* [Assist BG with MR](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/633).
* [Assist VS with MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68369).
* [Assist MP with MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68372).
* Slack re release notes problems.
* Slack re finance handbook.
* Expense reports.

# TUE 2020-11-17

##### Notes

* Documented after the fact so I'm missing a lot of work items.

##### Meetings

* DS create release video.

##### Work

* [Assist BG with MR](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/633).
* [Assist VS with MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68369).
* Assist JG with homepage.

# MON 2020-11-16

##### Notes

* Forgot to document work today.

##### Meetings

* CMS sprint plan video.
* CMS sprint kickoff.
* DS release candidate confirmation.

# FRI 2020-11-13

##### Work

* [Pricing refinement](https://gitlab.com/gitlab-com/marketing/growth-marketing/marketing-website/-/issues/37).
* [Homepage review](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/66787).
* [Enterprise release candidate](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/65214).
* [Review MR for add CMS admin](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/67698).
* Assist Ops via slack with Marketo template issue.
* Slack re TB review video of upcoming work re homepage and enterprise.

# THU 2020-11-12

##### Meetings

* Sprint recap.
* Weekly marketing.
* Interview sync.

##### Work

* [Reorganize CMS epic](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/142).
* Enterprise page.
* Assist vendors via slack.
* Slack re issue status labels.
* Slack re CSS frameworks.
* Slack re tender Q & A.

> In theory, it would be great if we used all of them, but in practice they are not updated. Therefore, we simplified while working through the process. In the future, we may add more back in but right now: Triage pulls them into the triage board and WIP moves them into sprint planning.

# WED 2020-11-11

##### Meetings

* Smartling sync.
* DT coffee chat.

##### Work

* Slack re CSS frameworks.
* Rewrote issue for CSS framework.
* [Review homepage MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/66787).
* Enterprise page.

# TUE 2020-11-10

##### Meetings

* Tender Q & A.
* CMS sprint kickoff.

##### Work

* Created issue for CSS framework.
* Issue catchup and refinement after VTO.
* [Topic page sidebars](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/640).
* [Add CTA button option to event pages](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/595).

# MON 2020-11-09

##### Notes

* Worked an extra 90 minutes.

##### Meetings

* Interview candidate.
* Sprint planning, old style.
* Interview candidate.
* MP sync.
* Interview candidate.
* LB sync.
* Sprint planning, new style.

##### Work

* [Created issue for topic sidebars phase 2](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/640).
* Meetings all day non-stop.

# 2020-11-02

##### Notes

* PTO all week

# [Previous month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/10.md) || [Next month](https://gitlab.com/brandon_lyon/daily-log/-/blob/master/2020/12.md)
